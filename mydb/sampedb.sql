-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 13, 2022 at 03:01 PM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sampedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `venue` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `image`, `date`, `venue`, `price`, `capacity`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Holi 2018', 'IaT0lvtVdPpRx9my31cAr0apwEIoqXS9672GFK7V.jpeg', '2018-05-18', 'Hotel Garden Area', 2500, 550, NULL, 1, '2022-04-22 11:32:38', '2022-05-04 14:11:22'),
(2, 'Food Festa', 'Kg4e1KIbXc5qo8kOHRJellaUyBg3QsLHsfbo10jT.jpeg', '2018-04-18', 'Hotel Garden Area', 3500, 350, 'Cultural Performance with food testing sessions.', 1, '2022-04-22 11:32:38', '2022-05-04 14:12:01'),
(3, 'South Asian Youth Summit', 'TCA4X9GiM81RpMFkpdj1l8LCMmSuJaJE9oJzecMV.jpeg', '2018-04-20', 'Leonat Conference Hall', 0, 250, 'Discussion of youth involvement in protection of cultural heritage.', 1, '2022-04-22 11:32:38', '2022-05-04 14:12:32'),
(4, 'Regional Minority Society Conference', '4.jpg', '2018-04-15', 'Darfurd Conference Hall', 0, 100, 'Leaders of minority society discuss the involvement in politics.', 1, '2022-04-22 11:32:38', NULL),
(5, 'Bankers Society Annual Summit', '5.jpg', '2018-04-10', 'Kafe Conference Hall', 5400, 60, 'Talks by industry veterans on Central Bank limits on foreign loans.', 0, '2022-04-22 11:32:38', '2022-05-04 14:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `event_bookings`
--

CREATE TABLE `event_bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `number_of_tickets` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `payment` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `name`, `icon`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Air Conditioner', 'air_conditioner.png', 1, '2022-04-22 11:32:37', NULL),
(2, 'Bathtub', 'bathtub.png', 1, '2022-04-22 11:32:37', NULL),
(3, 'Breakfast', 'breakfast.png', 1, '2022-04-22 11:32:37', NULL),
(4, 'Computer', 'computer.png', 1, '2022-04-22 11:32:37', NULL),
(5, 'First Aid Kit', 'first_aid_kit.png', 1, '2022-04-22 11:32:37', NULL),
(6, 'Hair Dryer', 'hair_dryer.png', 1, '2022-04-22 11:32:37', NULL),
(7, 'Mini Bar', 'mini_bar.png', 1, '2022-04-22 11:32:37', NULL),
(8, 'Indoor cooking (Optional)', 'mini_cooler.png', 1, '2022-04-22 11:32:37', '2022-04-27 06:03:49'),
(9, 'Parking', 'parking.png', 1, '2022-04-22 11:32:37', NULL),
(10, 'Telephone', 'telephone.png', 1, '2022-04-22 11:32:37', NULL),
(11, 'Television', 'television.png', 1, '2022-04-22 11:32:37', NULL),
(12, 'Wifi', 'wifi.png', 1, '2022-04-22 11:32:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facility_room_type`
--

CREATE TABLE `facility_room_type` (
  `facility_id` int(10) UNSIGNED NOT NULL,
  `room_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facility_room_type`
--

INSERT INTO `facility_room_type` (`facility_id`, `room_type_id`, `created_at`, `updated_at`) VALUES
(8, 1, '2022-04-22 11:32:37', NULL),
(9, 1, '2022-04-22 11:32:37', NULL),
(10, 1, '2022-04-22 11:32:37', NULL),
(11, 1, '2022-04-22 11:32:37', NULL),
(12, 1, '2022-04-22 11:32:37', NULL),
(1, 2, '2022-04-22 11:32:38', NULL),
(2, 2, '2022-04-22 11:32:38', NULL),
(3, 2, '2022-04-22 11:32:38', NULL),
(4, 2, '2022-04-22 11:32:38', NULL),
(5, 2, '2022-04-22 11:32:38', NULL),
(6, 2, '2022-04-22 11:32:38', NULL),
(7, 2, '2022-04-22 11:32:38', NULL),
(8, 2, '2022-04-22 11:32:38', NULL),
(9, 2, '2022-04-22 11:32:38', NULL),
(10, 2, '2022-04-22 11:32:38', NULL),
(1, 3, '2022-04-22 11:32:38', NULL),
(2, 3, '2022-04-22 11:32:38', NULL),
(3, 3, '2022-04-22 11:32:38', NULL),
(4, 3, '2022-04-22 11:32:38', NULL),
(9, 3, '2022-04-22 11:32:38', NULL),
(10, 3, '2022-04-22 11:32:38', NULL),
(12, 3, '2022-04-22 11:32:38', NULL),
(1, 4, '2022-04-22 11:32:38', NULL),
(2, 4, '2022-04-22 11:32:38', NULL),
(3, 4, '2022-04-22 11:32:38', NULL),
(4, 4, '2022-04-22 11:32:38', NULL),
(8, 4, '2022-04-22 11:32:38', NULL),
(2, 5, '2022-04-22 11:32:38', NULL),
(5, 5, '2022-04-22 11:32:38', NULL),
(7, 5, '2022-04-22 11:32:38', NULL),
(9, 5, '2022-04-22 11:32:38', NULL),
(8, 6, '2022-04-22 11:32:38', NULL),
(11, 6, '2022-04-22 11:32:38', NULL),
(12, 6, '2022-04-22 11:32:38', NULL),
(10, 4, '2022-04-22 11:32:38', NULL),
(1, 1, '2022-04-27 06:52:17', '2022-04-27 06:52:17'),
(5, 1, '2022-04-27 06:52:17', '2022-04-27 06:52:17');

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE `foods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Appetizer','Soup','Salad','Main Course','Dessert') COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `name`, `type`, `image`, `price`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sizzling Gambas', 'Appetizer', 'MJxhNkWSKqNVOQfwT4ci8rutgbQUQ0ueuQvyo0dO.jpeg', '630.00', 'Sizzling gambas is made with a combination of shrimp and vegetables.', 1, '2022-04-22 11:32:38', '2022-05-04 14:22:56'),
(2, 'Calamares', 'Appetizer', '18SaJ4Nj78EkXEU2IZSVZryYgPITniZiDO7YwkkQ.jpeg', '630.00', 'Calamares is the Filipino version of the Mediterranean breaded fried squid dish, Calamari.', 1, '2022-04-22 11:32:38', '2022-05-04 14:23:09'),
(3, 'Tinolang Manok', 'Soup', 'tinolang_manok.jpg', '530.00', 'Tinola in Tagalog or Visayan, is a soup-based dish served as an main dish in the Philippines.', 0, '2022-04-22 11:32:38', '2022-05-04 14:23:17'),
(4, 'Chicken Sotanghon Soup', 'Soup', 'chicken_sotanghon_soup.jpg', '410.00', 'Chicken Sotanghon Soup is a soup made with bite-sized chicken, cellophane noodles and vegetables.', 0, '2022-04-22 11:32:38', '2022-05-04 14:23:26'),
(5, 'Mixed Green Salad', 'Salad', 'mixed_green_salad.jpg', '370.00', 'Garlic, crushed red pepper flakes season the light vinaigrette that dresses this refreshing salad.', 0, '2022-04-22 11:32:38', '2022-05-04 14:24:58'),
(6, 'Chef\'s Salad', 'Salad', 'chef_salad.jpg', '400.00', 'Chef salad is an American salad consisting of eggs, meat, chicken, tomatoes, cucumbers and cheese.', 0, '2022-04-22 11:32:38', '2022-05-04 14:25:05'),
(7, 'Beefsteak Tagalog', 'Main Course', 'beefsteak_tagalog.jpg', '650.00', 'Beefsteak Tagalog is a dish of pieces of salted and peppered sirloin.', 0, '2022-04-22 11:32:38', '2022-05-04 14:25:12'),
(8, 'Cordon Bleu', 'Main Course', 'cordon_bleu.jpg', '630.00', 'A cordon bleu is a dish of meat wrapped around cheese, then breaded and pan-fried or deep-fried.', 0, '2022-04-22 11:32:38', '2022-05-04 14:25:18'),
(9, 'Chicken Pork Adobo', 'Main Course', 'chicken_pork_adobo.jpg', '630.00', 'Chicken Pork Adobo is a version of classic Filipino stew combining chicken pieces and pork cubes.', 0, '2022-04-22 11:32:38', '2022-05-04 14:26:10'),
(10, 'Grilled Squid', 'Main Course', 'grilled_squid.jpg', '550.00', 'The simple grilled squid recipe uses a fantastic cumin marinade for a Middle Eastern twist.', 0, '2022-04-22 11:32:38', '2022-05-04 14:26:21'),
(11, 'Fresh Fruit Platter', 'Dessert', 'fresh_fruit_platter.jpg', '300.00', 'It is a base of ripe, colorful, sliced melons and pineapples.', 0, '2022-04-22 11:32:38', '2022-05-04 14:26:32'),
(12, 'Banana Split', 'Dessert', 'banana_split.jpg', '360.00', 'A banana split is an ice cream-based dessert.', 0, '2022-04-22 11:32:38', '2022-05-04 14:26:47'),
(13, 'Chocolate Vanilla Sundae', 'Dessert', 'chocolate_vanilla_sundae.jpeg', '200.00', 'This is a rich sundae made with brownies, vanilla ice cream, chocolate syrup, and whipped cream.', 0, '2022-04-22 11:32:38', '2022-05-04 14:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `food_orders`
--

CREATE TABLE `food_orders` (
  `food_id` int(10) UNSIGNED NOT NULL,
  `room_booking_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `cost` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `room_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `caption`, `is_primary`, `status`, `room_type_id`, `created_at`, `updated_at`) VALUES
(1, 'h2LPkiQw12NgDmYI9d8kna5ZX7X8l0uxswlH4Ysm.jpeg', 'Bright Room', 1, 1, 1, '2022-04-22 11:32:37', '2022-05-04 14:00:24'),
(2, 'aFIjT1JIS4eXw0VHY0elnClSabQjFnDKlsXsCL7d.jpeg', 'Out View', 0, 1, 1, '2022-04-22 11:32:37', '2022-05-04 14:02:26'),
(3, '3.jpg', 'Swimming in the Dusk', 0, 0, 1, '2022-04-22 11:32:37', '2022-05-04 14:02:16'),
(4, 'O6YC7Kk232hrvHjCeHeAADPHNoFFBKJTc1obE5C4.jpeg', 'A fine Winter', 1, 1, 2, '2022-04-22 11:32:37', '2022-05-04 14:09:18'),
(5, '5.jpg', 'Minimal', 0, 0, 2, '2022-04-22 11:32:37', '2022-05-04 14:09:18'),
(6, '6.jpg', 'Abstract', 0, 0, 2, '2022-04-22 11:32:37', '2022-05-04 14:09:18'),
(7, '4W78czmSSijeoUJN0ZpPm4vMyxb5BIxz8XM4srpy.jpeg', 'New Concept', 1, 1, 3, '2022-04-22 11:32:37', '2022-05-04 14:20:23'),
(8, '8.jpg', 'New Concept', 0, 1, 3, '2022-04-22 11:32:37', '2022-05-04 14:20:23'),
(9, '9.jpg', 'Best Concept', 0, 1, 3, '2022-04-22 11:32:37', '2022-05-04 14:20:23'),
(10, '10.jpg', 'New thing', 1, 1, 4, '2022-04-22 11:32:37', NULL),
(11, '11.jpg', 'Room with cool aspects', 0, 1, 4, '2022-04-22 11:32:37', NULL),
(12, '12.jpg', 'Amazing Room', 0, 1, 4, '2022-04-22 11:32:37', NULL),
(13, '13.jpg', 'Room with ac', 1, 1, 5, '2022-04-22 11:32:37', NULL),
(14, '14.jpg', 'Cozy Room', 0, 1, 5, '2022-04-22 11:32:37', NULL),
(15, '15.jpg', 'Artful room', 0, 1, 5, '2022-04-22 11:32:37', NULL),
(16, '16.jpg', 'Sculpture Room', 1, 1, 6, '2022-04-22 11:32:37', NULL),
(17, '17.jpg', 'Bath Room', 0, 1, 6, '2022-04-22 11:32:37', NULL),
(18, '18.jpg', 'Cooler Room', 0, 1, 6, '2022-04-22 11:32:37', NULL),
(19, '19.jpg', 'Whats new', 0, 0, 2, '2022-04-22 11:32:37', '2022-05-04 14:09:18'),
(20, '20.jpg', 'Summer Breeze', 0, 0, 2, '2022-04-22 11:32:37', '2022-05-04 14:09:18'),
(21, '21.jpg', 'Autumn Breeze', 0, 1, 3, '2022-04-22 11:32:37', '2022-05-04 14:20:23'),
(22, '22.jpg', 'New Breeze', 0, 0, 1, '2022-04-22 11:32:37', '2022-05-04 14:02:35'),
(23, '23.jpg', 'Full Breeze', 0, 0, 1, '2022-04-22 11:32:37', '2022-05-04 14:02:42'),
(24, '24.jpg', 'Coz Breeze', 0, 0, 1, '2022-04-22 11:32:37', '2022-05-04 14:03:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_29_024654_create_slider_table', 1),
(4, '2018_03_29_024713_create_facilities_table', 1),
(5, '2018_03_29_024753_create_room_types_table', 1),
(6, '2018_03_29_024939_create_facility_room_type_table', 1),
(7, '2018_03_29_025055_create_images_table', 1),
(8, '2018_03_29_025121_create_rooms_table', 1),
(9, '2018_03_29_025157_create_room_bookings_table', 1),
(10, '2018_03_29_025158_create_reviews_table', 1),
(11, '2018_03_29_031146_create_foods_table', 1),
(12, '2018_03_29_031207_create_food_orders_table', 1),
(13, '2018_04_07_022022_create_events_table', 1),
(14, '2018_04_08_025158_create_event_bookings_table', 1),
(15, '2018_05_06_035355_create_pages_table', 1),
(16, '2018_05_06_050318_create_subscribers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_title` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `url_title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About', 'about', 'Gezat Park is a modern living space with\nluxurious apartments and an excellent\nrestaurant. Situated in Bagamoyo, East\nAfrica’s most historic coastal town just\noutside Tanzanians busiest city of Dar es\nsalaam. It’s a perfect getaway or\npermanent stay.\nEnjoy the sunny afternoons in the pool with\nfood and drinks from our very own\nrestaurant,a magical evening walk along\nthe beach closeby or a weekend in the\nhistorical sites around the bagamoyo\ntown, Gezat park is indeed your home\naway from home', 1, '2022-04-22 11:32:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` enum('0','1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL,
  `approval_status` enum('pending','approved','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `room_booking_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_number` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `room_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_number`, `description`, `available`, `status`, `room_type_id`, `created_at`, `updated_at`) VALUES
(1, '1', 'Cumque expedita aperiam provident enim qui praesentium tenetur. Recusandae et in vel beatae dolor eius nam. Sint doloremque recusandae quaerat vel. Laudantium accusamus impedit quia itaque inventore.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(2, '2', 'Facere optio quisquam quae qui est. Enim doloremque asperiores ullam dicta natus et voluptatem. At tenetur dicta doloribus qui tenetur reprehenderit ut.', 1, 1, 2, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(3, '3', 'Non nobis sit nihil eligendi delectus culpa aut. Optio modi illum dolor non. Quae id repellat repellat iste possimus tempora. A aut excepturi ipsa iure debitis. Iure consectetur minus qui.', 1, 1, 2, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(4, '4', 'Earum officiis error architecto. Eveniet et et aliquam totam enim est necessitatibus. Et est et enim deleniti dolorem sunt voluptatibus.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(5, '5', 'Nobis totam maxime facere possimus et. Autem nemo ex laborum occaecati repellendus. Omnis quibusdam ut nobis.', 1, 1, 3, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(6, '6', 'Omnis nulla ipsam et nobis quia distinctio ut quas. Facilis esse placeat facere quas. Tenetur quia a esse iusto amet corrupti unde.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(7, '7', 'Ut rerum a id pariatur dignissimos quo. Voluptatem facere debitis alias voluptates. Facere et porro nihil.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(8, '8', 'Qui asperiores sit ex aut. Quasi atque quas earum molestiae consectetur eius consectetur. Ut tempora et sit ipsam laborum est.', 1, 1, 3, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(9, '9', 'Et quia perspiciatis aut velit. Debitis porro odio minus iste. Voluptatem est hic nihil nulla eius voluptatem perferendis. Aspernatur libero debitis soluta id.', 1, 1, 3, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(10, '10', 'Ut quia hic adipisci aliquid voluptas nulla voluptate est. Quia quis vitae quo sapiente consequuntur quam odio.', 1, 1, 5, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(11, '11', 'Dolores eum et laudantium mollitia aperiam. Quia ullam velit et nisi. Nisi provident error explicabo ad velit. Quo minima placeat aut eius quo.', 1, 1, 3, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(12, '12', 'Magni modi cupiditate hic tenetur aut. Suscipit commodi nihil et aperiam voluptate voluptate. Omnis quae dolorem nesciunt aut minus tempore.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(13, '13', 'Consequuntur veniam eos ipsa sunt eius. Possimus tempora repellendus explicabo et. Dolorem atque iure odio repudiandae.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(14, '14', 'Adipisci numquam necessitatibus magnam omnis sed et possimus. Excepturi mollitia molestiae saepe. Tempore vero quis aut consequatur neque itaque.', 1, 1, 6, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(15, '15', 'Deleniti earum nemo asperiores eius rem tenetur. Assumenda natus maxime alias molestiae. Libero non nulla at veritatis deleniti quo. Impedit est non hic hic impedit.', 1, 1, 1, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(16, '16', 'Ratione debitis dolor asperiores est fuga quia. Voluptatem quaerat sint quia nesciunt fuga accusamus. Explicabo sed autem tempora voluptatem.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(17, '17', 'In in facere dolorum qui asperiores vel. Voluptatum odit et sit debitis omnis. Dignissimos sed in eum incidunt quibusdam. Ipsam quibusdam ut harum quae.', 1, 1, 2, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(18, '18', 'Facilis iste quo mollitia qui ea. Rerum iure voluptatem quidem numquam voluptatum aperiam. Voluptatibus praesentium eveniet neque dolore impedit. Dolorem ullam dolorem consectetur porro dicta.', 1, 1, 5, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(19, '19', 'Cumque vel dolorem voluptas culpa voluptatum vel. Aliquid voluptas voluptatem cupiditate rerum illo sit. Vitae illo sed distinctio quo error aut incidunt. Sunt et quibusdam qui et.', 1, 1, 5, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(20, '20', 'At id quisquam rerum hic corporis ullam. Porro voluptatibus dolores culpa eos qui quod quas. Quia et repudiandae rerum at quo quo quo.', 1, 1, 3, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(21, '21', 'Dicta reiciendis autem corrupti provident et excepturi. Quia deleniti quisquam molestiae et voluptas quia exercitationem.', 1, 1, 2, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(22, '22', 'Nihil non sint ad voluptates eum quibusdam qui. Velit error excepturi consequatur delectus ad. Nisi ex adipisci consequatur autem modi eos in. Exercitationem sit quis tenetur suscipit ducimus.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(23, '23', 'Unde dolor ut voluptates maxime. Illo quia laboriosam perferendis. Dolorem eligendi architecto asperiores assumenda adipisci nihil.', 1, 1, 6, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(24, '24', 'Dolorem dolorem qui nihil fugit fugit. Deleniti eaque velit sit quia eligendi. Deserunt sint non rerum. Earum soluta quia quo eum vitae.', 1, 1, 1, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(25, '25', 'Sed aliquid distinctio qui eum consectetur minus aut. Possimus iusto quas sed ducimus tenetur. Placeat quidem velit nam dolorem. Asperiores unde fuga illo sunt enim et.', 1, 1, 2, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(26, '26', 'Molestias laborum nesciunt est animi ea asperiores tenetur. Non est odit vel et aut. Et ut magnam atque ipsum voluptatem dolores. Vero et quibusdam et est laboriosam cupiditate incidunt veniam.', 1, 1, 3, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(27, '27', 'Ab et facere tenetur odio amet dolor. Fuga placeat quidem omnis aperiam est id qui. Quaerat quod ducimus sapiente voluptatibus eum tempora qui. Sit voluptatem cupiditate quidem veritatis.', 1, 1, 5, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(28, '28', 'Voluptas maiores suscipit nihil facere. Dolores et voluptas dolor aperiam facilis. Qui rem sit aperiam qui quo.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(29, '29', 'Sapiente nisi nemo facere nesciunt voluptatem sed. Nam dignissimos error dolorum laborum nemo. Et veniam voluptatem enim vel natus.', 1, 1, 4, '2022-04-22 11:32:38', '2022-04-22 11:32:38'),
(30, '30', 'Sit et quam accusantium suscipit ut et deleniti. Alias consequatur nihil omnis qui ut. Nihil rerum repellat eum aspernatur non dolor.', 1, 1, 6, '2022-04-22 11:32:38', '2022-04-22 11:32:38');

-- --------------------------------------------------------

--
-- Table structure for table `room_bookings`
--

CREATE TABLE `room_bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `arrival_date` date NOT NULL,
  `departure_date` date DEFAULT NULL,
  `room_cost` int(11) NOT NULL,
  `status` enum('pending','checked_in','checked_out','cancelled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `payment` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_bookings`
--

INSERT INTO `room_bookings` (`id`, `room_id`, `user_id`, `arrival_date`, `departure_date`, `room_cost`, `status`, `payment`, `created_at`, `updated_at`) VALUES
(10, 15, 1, '2022-04-27', '2022-04-28', 298320, 'pending', 0, '2022-04-27 06:56:35', '2022-04-27 06:56:35'),
(13, 15, 15, '2022-04-29', '2022-04-30', 298320, 'cancelled', 0, '2022-04-29 06:41:17', '2022-04-29 11:22:01'),
(14, 24, 15, '2022-04-29', '2022-04-30', 298320, 'pending', 0, '2022-04-29 06:59:29', '2022-04-29 06:59:29'),
(15, 15, 15, '2022-05-01', '2022-05-06', 1491600, 'pending', 0, '2022-04-29 09:36:13', '2022-04-29 09:36:13'),
(16, 15, 15, '2022-06-02', '2022-06-02', 298320, 'pending', 0, '2022-04-29 11:23:05', '2022-04-29 11:23:05');

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

CREATE TABLE `room_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_per_day` int(11) NOT NULL,
  `discount_percentage` int(11) NOT NULL DEFAULT '0',
  `size` int(11) DEFAULT NULL,
  `max_adult` int(11) DEFAULT '0',
  `max_child` int(11) DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `room_service` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_types`
--

INSERT INTO `room_types` (`id`, `name`, `cost_per_day`, `discount_percentage`, `size`, `max_adult`, `max_child`, `description`, `room_service`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Two Bedroom Apartments', 240000, 0, 3000, 10, 5, 'Comfortable cotton bed shits with a wide bed and sleepy head. The room temperature is at 20`c how more comfortable can it get? Our Rooms\r\nhave an Air conditioner, Comfy Bed, dressing Table and Cupboard.', 1, 1, '2022-04-22 11:32:37', '2022-04-27 07:27:04'),
(2, 'Bedroom Apartments', 240000, 0, 2000, 8, 4, 'Maecenas erat lorem, vulputate sed ex at, vehicula dignissim risus. Nullam non nisi congue elit cursus tempus. Nunc vel ante nec libero semper maximus. Donec cursus sed massa eget commodo. Phasellus semper neque id iaculis malesuada. Nulla efficitur dui vitae orci blandit tempor. Mauris sed venenatis nibh, sed sodales risus.\r\n\r\nNam sit amet tortor in elit fermentum consectetur et sit amet eros. Sed varius velit at eros tempor sodales. Fusce at enim at lectus sollicitudin pharetra at in risus. Donec ut semper turpis. Maecenas lobortis ante ut eros scelerisque, at semper augue ullamcorper.', 1, 1, '2022-04-22 11:32:37', '2022-05-04 14:10:07'),
(3, 'Ultra Deluxe', 14000, 0, 1400, 5, 2, 'Maecenas erat lorem, vulputate sed ex at, vehicula dignissim risus. Nullam non nisi congue elit cursus tempus. Nunc vel ante nec libero semper maximus. Donec cursus sed massa eget commodo. Phasellus semper neque id iaculis malesuada. Nulla efficitur dui vitae orci blandit tempor. Mauris sed venenatis nibh, sed sodales risus.\r\n\r\nNam sit amet tortor in elit fermentum consectetur et sit amet eros. Sed varius velit at eros tempor sodales. Fusce at enim at lectus sollicitudin pharetra at in risus. Donec ut semper turpis. Maecenas lobortis ante ut eros scelerisque, at semper augue ullamcorper.', 1, 1, '2022-04-22 11:32:37', '2022-04-22 11:32:37'),
(4, 'Luxury Room', 9000, 0, 800, 4, 2, 'Maecenas erat lorem, vulputate sed ex at, vehicula dignissim risus. Nullam non nisi congue elit cursus tempus. Nunc vel ante nec libero semper maximus. Donec cursus sed massa eget commodo. Phasellus semper neque id iaculis malesuada. Nulla efficitur dui vitae orci blandit tempor. Mauris sed venenatis nibh, sed sodales risus.\r\n\r\nNam sit amet tortor in elit fermentum consectetur et sit amet eros. Sed varius velit at eros tempor sodales. Fusce at enim at lectus sollicitudin pharetra at in risus. Donec ut semper turpis. Maecenas lobortis ante ut eros scelerisque, at semper augue ullamcorper.', 0, 0, '2022-04-22 11:32:37', '2022-05-04 14:21:48'),
(5, 'Premium Room', 6000, 0, 500, 3, 2, 'Maecenas erat lorem, vulputate sed ex at, vehicula dignissim risus. Nullam non nisi congue elit cursus tempus. Nunc vel ante nec libero semper maximus. Donec cursus sed massa eget commodo. Phasellus semper neque id iaculis malesuada. Nulla efficitur dui vitae orci blandit tempor. Mauris sed venenatis nibh, sed sodales risus.\r\n\r\nNam sit amet tortor in elit fermentum consectetur et sit amet eros. Sed varius velit at eros tempor sodales. Fusce at enim at lectus sollicitudin pharetra at in risus. Donec ut semper turpis. Maecenas lobortis ante ut eros scelerisque, at semper augue ullamcorper.', 0, 0, '2022-04-22 11:32:37', '2022-05-04 14:21:57'),
(6, 'Normal Room', 3000, 0, 300, 2, 1, 'Maecenas erat lorem, vulputate sed ex at, vehicula dignissim risus. Nullam non nisi congue elit cursus tempus. Nunc vel ante nec libero semper maximus. Donec cursus sed massa eget commodo. Phasellus semper neque id iaculis malesuada. Nulla efficitur dui vitae orci blandit tempor. Mauris sed venenatis nibh, sed sodales risus.\r\n\r\nNam sit amet tortor in elit fermentum consectetur et sit amet eros. Sed varius velit at eros tempor sodales. Fusce at enim at lectus sollicitudin pharetra at in risus. Donec ut semper turpis. Maecenas lobortis ante ut eros scelerisque, at semper augue ullamcorper.', 0, 0, '2022-04-22 11:32:37', '2022-05-04 14:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_title` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `big_title` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_text` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `name`, `small_title`, `big_title`, `description`, `link`, `link_text`, `status`, `created_at`, `updated_at`) VALUES
(1, 'lwk92lw8pceuyNMj1TfaFCe1jvYtdWdJUToCghPY.jpeg', NULL, 'Gezat Park hotel and Apartment', NULL, 'room_type/1', 'Book Now', 1, '2022-04-22 11:32:37', '2022-05-04 14:37:15'),
(2, 'ukJANMGlugbA3yLimkS9hm2eR03AhhRHIdANkgZz.jpeg', NULL, 'Gezat Park hotel and Apartment', NULL, 'room_type/2', 'Book Now', 1, '2022-04-22 11:32:37', '2022-05-04 14:37:57'),
(3, 'gF9iucYZaZQL0CgOyPTFA1BZABlLfC4KsDg9rh9u.jpeg', NULL, 'Gezat Park hotel and Apartment', NULL, 'room_type/3', 'Book Now', 1, '2022-04-22 11:32:37', '2022-05-04 14:40:48'),
(4, '4.jpg', 'Four Experience the hotel', 'Four Gezat Park Gardens', 'Four Mauris non placerat nulla. Sed vestibulum quam mauris, et malesuada tortor venenatis sem porta est consectetur posuere. Praesent nisi velit, porttitor ut imperdiet a, pellentesque id mi.', 'room_type/4', 'Book Now', 0, '2022-04-22 11:32:37', '2022-04-27 04:31:36');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'edgar.aidan@simbalogistic.co.tz', '2022-04-22 11:56:12', '2022-04-22 11:56:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `gender`, `phone`, `address`, `email`, `password`, `avatar`, `about`, `facebook_id`, `twitter_id`, `google_id`, `role`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Davie', 'Davisino', 'male', '9866893439', 'Kathmandu', 'dhsoft@gmail.com', '$2y$10$kMtMonu2Fv/qFCSbIfkhJe1EUkeabQZF6IGfbcIP8h.JSwfMCSS6y', 'vhEq3B003tvouOREz3QDKojO5EMIxKNaqz1dldJz.png', 'hello from the other world', NULL, NULL, NULL, 'admin', 1, 'lFxDa9rwzDZm7wiqCH8E0kNCIZfUG57Eu4kpkEFqYHny4ze4W3jQlxqxsGrj', '2022-04-22 11:32:36', '2022-04-22 11:47:01'),
(15, 'David', 'John', 'male', NULL, NULL, 'davidhaule3@gmail.com', '$2y$10$NHN.XAd/Lc0B18Jhij6die124/B5yfzdqbFpFIPvhjS7ESFDOtH.u', 'man.png', NULL, NULL, NULL, NULL, 'user', 1, NULL, '2022-04-29 06:40:58', '2022-04-29 06:40:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `events_name_unique` (`name`);

--
-- Indexes for table `event_bookings`
--
ALTER TABLE `event_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_bookings_user_id_index` (`user_id`),
  ADD KEY `event_bookings_event_id_index` (`event_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `facilities_name_unique` (`name`);

--
-- Indexes for table `facility_room_type`
--
ALTER TABLE `facility_room_type`
  ADD KEY `facility_room_type_facility_id_index` (`facility_id`),
  ADD KEY `facility_room_type_room_type_id_index` (`room_type_id`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_orders`
--
ALTER TABLE `food_orders`
  ADD KEY `food_orders_food_id_index` (`food_id`),
  ADD KEY `food_orders_room_booking_id_index` (`room_booking_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_room_type_id_index` (`room_type_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_room_booking_id_index` (`room_booking_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rooms_room_number_unique` (`room_number`),
  ADD KEY `rooms_room_type_id_index` (`room_type_id`);

--
-- Indexes for table `room_bookings`
--
ALTER TABLE `room_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_bookings_room_id_index` (`room_id`),
  ADD KEY `room_bookings_user_id_index` (`user_id`);

--
-- Indexes for table `room_types`
--
ALTER TABLE `room_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_types_name_unique` (`name`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_facebook_id_unique` (`facebook_id`),
  ADD UNIQUE KEY `users_twitter_id_unique` (`twitter_id`),
  ADD UNIQUE KEY `users_google_id_unique` (`google_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `event_bookings`
--
ALTER TABLE `event_bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `foods`
--
ALTER TABLE `foods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `room_bookings`
--
ALTER TABLE `room_bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `room_types`
--
ALTER TABLE `room_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event_bookings`
--
ALTER TABLE `event_bookings`
  ADD CONSTRAINT `event_bookings_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `event_bookings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `facility_room_type`
--
ALTER TABLE `facility_room_type`
  ADD CONSTRAINT `facility_room_type_facility_id_foreign` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `facility_room_type_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `food_orders`
--
ALTER TABLE `food_orders`
  ADD CONSTRAINT `food_orders_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `food_orders_room_booking_id_foreign` FOREIGN KEY (`room_booking_id`) REFERENCES `room_bookings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_room_booking_id_foreign` FOREIGN KEY (`room_booking_id`) REFERENCES `room_bookings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `room_bookings`
--
ALTER TABLE `room_bookings`
  ADD CONSTRAINT `room_bookings_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `room_bookings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
